angular.module('model.DaftarPemakai', ['ionic'])

// Factory untuk memanggil Web Service dengan kembalian JSON data yang diminta
.factory('ambilDaftarPemakai', function($http) {
    var daftarPemakai = [];

    return {
        hapus: function(id) {
            daftarPemakai.splice(id, 1);
        }, // hapus
        setPemakai: function(id, depan, belakang) {
            daftarPemakai[id].name.first = depan;
            daftarPemakai[id].name.last = belakang;
        }, // setPemakai
        getPemakai: function(id) {
            return daftarPemakai[id];
        }, // getPemakai
        getDaftarPemakai: function() {
            return $http.get('https://randomuser.me/api/?results=10')
                        .then(function(response) {
                            daftarPemakai = response.data.results;
                            return daftarPemakai;
                        }); // .then
        } // getDaftarPemakai
    } // Anonymous function
});
