/*
 * Module: starter
 */
angular.module('starter', ['ionic', 'atur.DaftarPemakai'])

// run: di Angular semacam main kalau di Java atau C. Parameternya adalah
// callback function saat run dipanggil.
.run(function($ionicPlatform) {
    // ready: fungsi yang akan dipanggil saat perangkat siap untuk menjalankan
    // aplikasi ini. Parameternya adalah callback function saat ready dipanggil.
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

// Router default aplikasi
.config(function($urlRouterProvider){
    $urlRouterProvider.otherwise('/daftar');
});
