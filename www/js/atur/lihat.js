angular.module('atur.LihatPemakai', ['ionic', 'model.DaftarPemakai', 'atur.UbahPemakai'])

// Router untuk tampilan Lihat Pemakai
.config(function($stateProvider){
  $stateProvider
    .state('LihatPemakai', // Nama state ini
           { url: "/lihat/:id", // URL tampilan, ada parameter id
             templateUrl: "tampilan/lihat.html", // Template untuk daftar ini
             controller: "aturLihatPemakai"}); // Cotroller Tampilan ini
 })

// Controller untuk liat rincian pemakai
.controller("aturLihatPemakai", function($scope,
                                         $stateParams, // Untuk akses parameter
                                         $ionicPopup, // Untuk popup
                                         $state,
                                         ambilDaftarPemakai) { // Akses model
	$scope.id = $stateParams.id;
	$scope.pemakai = ambilDaftarPemakai.getPemakai($scope.id);

    $scope.konfirmasi = function() {
        var jawabanHapus = $ionicPopup.confirm({
            title: 'Konfirmasi',
            template: 'Yakin hapus pemakai: ' + $scope.pemakai.name.first + " "
                                              + $scope.pemakai.name.last + "?",
            okText : 'Ya',
            okType: 'button-positive',
            cancelText: 'Tidak',
            cancelType: 'button-assertive'
        })
        .then(function(res) {
            if (res) {
                $state.go("DaftarPemakai");
                ambilDaftarPemakai.hapus($scope.id);
            }
        });
    };
});
