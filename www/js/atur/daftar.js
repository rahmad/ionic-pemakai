angular.module('atur.DaftarPemakai', ['ionic', 'model.DaftarPemakai', 'atur.LihatPemakai'])

// Router untuk tampilan Daftar Pemakai
.config(function($stateProvider){
  $stateProvider
    .state('DaftarPemakai', // Nama state ini
           { url: "/daftar", // URL untuk akses tampilan ini
             templateUrl: "tampilan/daftar.html", // Template untuk daftar ini
             controller: "aturDaftarPemakai"}); // Cotroller Tampilan ini
 })

 // Controller untuk ion-list yang akan menampilkan daftar pemakai
 .controller("aturDaftarPemakai", function($scope, ambilDaftarPemakai) {
     ambilDaftarPemakai.getDaftarPemakai().then(function(daftarPemakai) {
         $scope.daftarPemakai = daftarPemakai;
     });
 });
