angular.module('atur.UbahPemakai', ['ionic', 'model.DaftarPemakai'])

// Router untuk tampilan Lihat Pemakai
.config(function($stateProvider){
    $stateProvider
    .state('UbahPemakai', // Nama state ini
           { cache:false,
             url: "/ubah/:id", // URL tampilan, ada parameter id
             templateUrl: "tampilan/ubah.html", // Template untuk daftar ini
             controller: "aturUbahPemakai"}); // Cotroller Tampilan ini
})

// Controller untuk liat rincian pemakai
.controller("aturUbahPemakai", function($scope,
                                        $stateParams, // Untuk akses parameter
                                        ambilDaftarPemakai) { // Akses model
	$scope.id = $stateParams.id;
    $scope.pemakai = ambilDaftarPemakai.getPemakai($scope.id);
    $scope.isian = {
        depan: $scope.pemakai.name.first,
        belakang : $scope.pemakai.name.last
    };

    $scope.ubah = function() {
        ambilDaftarPemakai.setPemakai($scope.id, $scope.isian.depan, $scope.isian.belakang);
    };
});
